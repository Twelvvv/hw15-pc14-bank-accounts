#pragma once
#include "BankAccount.h"

enum Status {INACTIVE, ACTIVE};

class SavingsAccount : public BankAccount {
	private:
		Status status;
	public:
		// constructor
		SavingsAccount(double, double);
		// mutators / accessors
		void setStatus(Status status)
			{ this->status = status; }
		bool getStatus()
			{ return status; }
		// member functions
		void updateStatus();
		void withdraw(double);
		void deposit(double);
		void monthlyProc();
		void updateMonthlyServiceCharges();
		void interact();
};
