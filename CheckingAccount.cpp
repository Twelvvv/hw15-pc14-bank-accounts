#include "CheckingAccount.h"

CheckingAccount::CheckingAccount(double b, double i) : BankAccount(b, i) {
	setMonthlyServiceCharges(getMonthlyServiceCharges() + 5.00);
}

void CheckingAccount::interact()
{
	BankAccount::interact("Checking");
}

void CheckingAccount::withdraw(double withdrawAmount)
{
	if(getBalance() - withdrawAmount < 0)
	{
		setMonthlyServiceCharges(getMonthlyServiceCharges() + 15.00);
		std::cout << "Account overdraft attempted, $15 fee applied." << std::endl;
	}
	else {
		BankAccount::withdraw(withdrawAmount);
		setMonthlyServiceCharges(getMonthlyServiceCharges() + .10);
	}
}
