# $@ = name of target
# $^ = names of all prerequisites, with spaces between them.
# $< = name of the first or only prerequisite
CXX	= g++
CXXFLAGS= -w -g -c
LDFLAGS	= -lm

TARGET	= a.exe

SRCEXT	= cpp

SRCFILES:= $(wildcard *.$(SRCEXT))
OBJFILES:= $(patsubst %.$(SRCEXT), %.o, $(notdir $(SRCFILES)))
OBJS := $(patsubst %.$(SRCEXT), %.o, $(notdir $(SRCFILES)))

RM	= rm -rf

all: run

run: $(TARGET)
	./$(TARGET)

$(TARGET): $(OBJFILES)
	$(CXX) $(LDFLAGS) $^ -o $@

%.o: %.cpp
	$(CXX) $< $(CXXFLAGS) -o $@

.PHONY: all run

clean:
	$(RM) *.o $(TARGET)
