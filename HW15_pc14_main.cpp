/*	Author:		Erik Zorer, Tim McCall, Justin Tupas
	Date:		05/07/2014
	Assignment: HW15_pc14 - Bank Account group project
	Time spent: 
*/
#include <iostream>
#include <string>
#include "CheckingAccount.h"
#include "SavingsAccount.h"

int main(int argc, char *argv[]) {
	using std::cout; using std::cin; using std::endl;

	// Create savings account
	cout << "\t\t**Savings**" << endl;
	double deposit_amount = 0;
	do {
		std::string input;
		cout << "Make an initial deposit: ";
		getline(cin, input);
		deposit_amount = atof(input.c_str());
	} while (deposit_amount == 0);
	SavingsAccount savings(deposit_amount, .05); // 5%
	// Allow user to make deposits / withdrawals
	savings.interact();

	// Create checking account
	cout << endl << endl
		 << "\t\t**Checking**" << endl;
	deposit_amount = 0;
	do {
		std::string input;
		cout << "Make an initial deposit: ";
		getline(cin, input);
		deposit_amount = atof(input.c_str());
	} while (deposit_amount == 0);
	CheckingAccount checking(deposit_amount, .02); // 2%
	// Allow user to make deposits / withdrawals
	checking.interact();
	
	system("pause");
	return 0;
}
