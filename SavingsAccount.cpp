#include "SavingsAccount.h"

// constructor
SavingsAccount::SavingsAccount(double balance, double annual_interest_rate)
			   :BankAccount(balance, annual_interest_rate) {
	updateStatus();
}

// member functions
void SavingsAccount::updateStatus() {
	if (getBalance() >= 25)
		setStatus(ACTIVE);
	else
		setStatus(INACTIVE);
}

void SavingsAccount::withdraw(double amount) {
	if (status == ACTIVE) {
		BankAccount::withdraw(amount);
		updateMonthlyServiceCharges();
		updateStatus();
	}
	else
		std::cout << "Account is inactive; Must have atleast $25 to withdraw.\n";
}

void SavingsAccount::deposit(double amount) {
	BankAccount::deposit(amount);
	updateStatus();
}

void SavingsAccount::monthlyProc() {
	BankAccount::monthlyProc();
	updateStatus();
}

void SavingsAccount::updateMonthlyServiceCharges() {
	int num_withdrawals = getNumberWithdrawalsThisMonth();
	if (num_withdrawals > 4) {
		setMonthlyServiceCharges(
			getMonthlyServiceCharges() + 1
		);
	}
}

void SavingsAccount::interact() {
	BankAccount::interact("Savings");
}
