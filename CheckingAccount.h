#pragma once
#include "BankAccount.h"

class CheckingAccount : public BankAccount {
	public:
		CheckingAccount(double, double);
		void interact();
		void withdraw(double);
};
