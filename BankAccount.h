#pragma once
#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>

using namespace std;

class BankAccount
{
private:
	double balance;
	double initialbalance;
	int deposits;
	int withdrawals;
	double interest;
	double charges;

public:
	// Constructor for Bank Account with Balance and interest rate arguments
	// that set their respective variables.
	BankAccount(double b, double i)
	{
		balance = b;
		initialbalance = b;
		interest = i;
		deposits = 0;
		withdrawals = 0;
		charges = 0;
	}

	virtual void deposit(double d)
	{
		if(d>0)
		{
			balance += d;
			deposits++;
		}
		else{ cout << "Invalid deposit amount." << endl;}
	}

	virtual void withdraw(double w)
	{
		if(w>0 && w <= balance)
		{
			balance -= w;
			withdrawals++;
		}
		else{ cout << "Invalid withdrawal amount." << endl;}
	}

	void calcInt()
	{
		double monthinterest = interest/(double)12;
		double currentmonthint = monthinterest * balance;
		balance += currentmonthint;
	}
	
	virtual void monthlyProc()
	{
		balance -= charges;
		calcInt();
		withdrawals = 0;
		deposits = 0;
		charges = 0;
	}
	
	void interact(string obj_name) 
	{ 
		cout << fixed << setprecision(2)
			 << obj_name << " balance: $" << getBalance() << endl;
		bool stop = false;
		do {
			double amount = 0;
			string input;
			do {
				cout << "Make a deposit (+) / withdrawal (-) ('q' to quit): ";
				getline(cin, input);
				if (input.compare("q") == 0)
					stop = true;
				else
					amount = atof(input.c_str());
			}while (amount == 0 && stop != true);
			if (amount > 0)
				deposit(amount);
			else if (amount < 0)
				withdraw(amount * -1);
			cout << obj_name << " balance: $" << getBalance() << endl;
		} while (!stop);

		cout << endl
			 << "\t\t" << obj_name << " Monthly Summary" << endl
			 << "Beginning balance: $" << getInitialBalance() << endl
			 << "Total deposits: " << getNumberDepositsThisMonth() << endl
			 << "Total withdrawals: " << getNumberWithdrawalsThisMonth() << endl
			 << "Service charges: $" << getMonthlyServiceCharges() << endl;
		monthlyProc();
		cout << "Ending balance: $" << getBalance() << endl;
	}

	double getInitialBalance()
	{
		return initialbalance;
	}

	int getNumberDepositsThisMonth()
	{
		return deposits;
	}
	int getNumberWithdrawalsThisMonth()
	{
		return withdrawals;
	}
	double getMonthlyServiceCharges()
	{
		return charges;
	}
	double getBalance()
	{
		return balance;
	}
	void setMonthlyServiceCharges(double m)
	{
		charges = m;
	}
	
	

};
